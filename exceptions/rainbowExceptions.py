class NoR6Token(Exception):
    pass


class R6NoUserFound(Exception):
    pass


class InvalidPlatform(Exception):
    pass


class TooManyRequests(Exception):
    pass
