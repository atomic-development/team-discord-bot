# Team Discord Bot (ABANDONED)
Team bot is no longer under development, the code became too messy and too inefficient, also due to the limitations of python, I no longer want to continue developing team bot!

## About:
Team bot is a discord bot designed for people who constantly are trying to find ways to sort their friends into teams without bias. This bot allows you to randomly sort users into a select number of teams for your game, competition etc.

## Commands:
All the commands can be found on the wiki [here](https://gitlab.com/AtomicProgramming/team-discord-bot/-/wikis/home), you can find the version of the discord bot through the status of the discord bot.

## Bugs/Issues:
If you run into any issues or bugs, please make a issue on this repository describing to the best of your ability what happened and steps to reproduce, I will make a bug report template when I get time, furthermore you may dm me on discord to report an issue `Destroyer#4570`.

Thank you for helping fix bugs :)

## Credits:
- **Destroyer/AtomicProgramming: Owner and Head Developer of Team Bot**
- **Fox (Discord: The Fox#1945): Team Bot logo/Icon**
