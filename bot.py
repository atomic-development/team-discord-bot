# This is the main file, run this file to run the entire program
from os import listdir
from sys import exit

import handlers

version = "v1.1.9"

try:  # attempts to import the discord module, as it must be downloaded through the python package manager
    import discord
    from discord.ext import commands
except ModuleNotFoundError:
    handlers.logger.fatal(
        "Discord module could not be found, please check if it is installed by running the command pip install discord")
    exit(1)


class Bot:  # Main class for the bot

    def __init__(self):
        self.client = commands.Bot(command_prefix=handlers.PrefixHandler.get_prefix)
        self.client.remove_command(
            "help")  # By default discord.py adds a help command, this must be removed to add a custom one
        for file in listdir("cogs"):
            if file.endswith(".py"):
                self.client.load_extension(
                    "cogs.{}".format(file[:-3]))  # Loads the cogs, removing the .py at the end of the filename

        discord_token = handlers.TokenHandler.get_discord_token()
        if discord_token is None:
            handlers.logger.fatal("Discord Token could not be found in the config file")
            exit(1)
        self.client.run(handlers.TokenHandler.get_discord_token())


if __name__ == "__main__":
    handlers.StartHandler.start()
    try:
        bot = Bot()
    except discord.errors.LoginFailure as error:
        handlers.logger.fatal("Discord bot failed to login to the discord API", exc_info=True)
