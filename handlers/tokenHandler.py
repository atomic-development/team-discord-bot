from handlers.loggingHandler import logger
from json import load


class TokenHandler:  # Handles the discord token used to contact the discord API

    @staticmethod
    def get_discord_token():
        try:
            with open("./Data/config.json", "r") as f:
                config = load(f)
            discord_token = config["discord_token"]
            return discord_token
        except KeyError:
            return None

    @staticmethod
    def get_r6_token():
        try:
            with open("./Data/config.json", "r") as f:
                config = load(f)
            r6token = config['r6_token']
            return r6token
        except KeyError:
            return None
