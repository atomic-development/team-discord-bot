from handlers.tokenHandler import TokenHandler
from handlers.loggingHandler import logger
from handlers.startHandler import StartHandler
StartHandler.start()
from handlers.sqliteHandler import MessageDataHandler
from handlers.sqliteHandler import guild_data
from handlers.sqliteHandler import RainbowStatisticsDatabaseHandler
from handlers.teams import Teams
from handlers.timeTakenHandler import TimeTakenHandler
from handlers.prefixHandler import PrefixHandler
from handlers.r6StatsHandler import R6StatsHandler
