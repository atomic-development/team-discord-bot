from handlers.loggingHandler import logger
from handlers import guild_data


class PrefixHandler:  # prefix handler class, used class for static methods as it keeps the functions all together

    @staticmethod
    def get_prefix(client_prefix, message):
        del client_prefix  # currently no use for client_prefix
        try:
            return guild_data.get_guild_prefix(message.guild.id)  # Return the prefix stored in the database
        except Exception as error:
            logger.warn(error)
