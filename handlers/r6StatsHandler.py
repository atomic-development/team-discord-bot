from datetime import timedelta
from time import time

import requests

from exceptions import NoR6Token
from exceptions import R6NoUserFound
from exceptions import DataNotFound
from exceptions import InvalidPlatform
from exceptions import TooManyRequests
from handlers import RainbowStatisticsDatabaseHandler
from handlers import TokenHandler
from handlers import logger

SCRAPE_INTERVAL = 7200


class R6StatsHandler:

    def __init__(self):
        self.stats_db_handler = RainbowStatisticsDatabaseHandler()

    def remaining_time(self, username, platform, type: str):  # type call should be implemented dynamically TODO!
        last_scrape = 0

        if type == "casual":
            last_scrape = self.stats_db_handler.get_last_casual_scrape(username, platform)

        if type == "unranked":
            last_scrape = self.stats_db_handler.get_last_unranked_scrape(username, platform)

        if type == "ranked":
            last_scrape = self.stats_db_handler.get_last_ranked_scrape(username, platform)

        current_time = int(time())
        remaining_time = (last_scrape + SCRAPE_INTERVAL) - current_time
        datetime = timedelta(seconds=remaining_time)
        return datetime

    def scrape_casual_stats(self, username, platform):
        last_scrape = self.stats_db_handler.get_last_casual_scrape(username, platform)
        if last_scrape is None:
            last_scrape = 0
        next_scrape = last_scrape + SCRAPE_INTERVAL
        current_unix_time = int(time())
        if current_unix_time >= next_scrape:
            token = TokenHandler.get_r6_token()
            if token is None:
                raise NoR6Token
            connection = requests.Session()
            connection.headers['authorization'] = "Bearer " + token
            data = connection.get(
                "https://api2.r6stats.com/public-api/stats/{0}/{1}/generic".format(username, platform))
            if data.status_code == 500 or data.status_code == 404:
                raise R6NoUserFound
            elif data.status_code == 400:
                raise InvalidPlatform
            elif data.status_code == 429:
                raise TooManyRequests
            data = data.json()
            casual_data = data['stats']['queue']['casual']
            data_dict = {'username': data['username'], 'platform': data['platform'],
                         'level': data['progression']['level'],
                         'kills': casual_data['kills'], 'deaths': casual_data['deaths'],
                         'games_played': casual_data['games_played'], 'kd': casual_data['kd'],
                         'wins': casual_data['wins'],
                         'losses': casual_data['losses'], 'wl': casual_data['wl']}
            current_unix_time = int(time())
            data_dict['last_update'] = current_unix_time
            self.stats_db_handler.add_casual_stats(data_dict)
            logger.debug("Scraped stats for user {} on the platform {}".format(username, platform))
        else:
            return

    def get_casual_stats(self, username, platform):
        data = self.stats_db_handler.get_casual_stats(username, platform)
        if data is None:
            raise R6NoUserFound
        else:
            return data

    def scrape_unranked_stats(self, username, platform):
        try:
            last_scrape = self.stats_db_handler.get_last_unranked_scrape(username, platform)
        except DataNotFound:
            last_scrape = 0
        current_time = int(time())
        next_scrape = last_scrape + 7200

        if current_time >= next_scrape:
            token = TokenHandler.get_r6_token()
            if token is None:
                raise NoR6Token
            connection = requests.Session()
            connection.headers['authorization'] = "Bearer " + token
            data = connection.get(
                "https://api2.r6stats.com/public-api/stats/{0}/{1}/generic".format(username, platform)
            )
            if data.status_code == 500 or data.status_code == 404:
                raise R6NoUserFound
            elif data.status_code == 400:
                raise InvalidPlatform
            elif data.status_code == 429:
                raise TooManyRequests
            data = data.json()
            unranked_data = data['stats']['queue']['other']
            data_dict = {'username': data['username'], 'platform': data['platform'],
                         'level': data['progression']['level'], 'kills': unranked_data['kills'],
                         'deaths': unranked_data['deaths'], 'games_played': unranked_data['games_played'],
                         'kd': unranked_data['kd'], 'wins': unranked_data['wins'], 'losses': unranked_data['losses'],
                         'wl': unranked_data['wl']}
            current_unix_time = int(time())
            data_dict['last_update'] = current_unix_time
            self.stats_db_handler.add_unranked_stats(data=data_dict)
            logger.debug("Scraped stats for user {} on the platform {}".format(username, platform))
        else:
            return

    def get_unranked_stats(self, username, platform):
        try:
            data = self.stats_db_handler.get_unranked_stats(username, platform)
        except DataNotFound:
            raise R6NoUserFound
        return data

    def scrape_ranked_stats(self, username, platform):
        try:
            last_scrape = self.stats_db_handler.get_last_ranked_scrape(username, platform)
        except DataNotFound:
            last_scrape = 0
        current_time = int(time())
        next_scrape = last_scrape + 7200

        if current_time >= next_scrape:
            token = TokenHandler.get_r6_token()
            if token is None:
                raise NoR6Token
            connection = requests.Session()
            connection.headers['authorization'] = "Bearer " + token
            generic = connection.get(
                "https://api2.r6stats.com/public-api/stats/{0}/{1}/generic".format(username, platform)
            )
            seasonal = connection.get(
                "https://api2.r6stats.com/public-api/stats/{0}/{1}/seasonal".format(username, platform)
            )
            codes = [generic, seasonal]
            for code in codes:
                if code.status_code == 500 or code.status_code == 404:
                    raise R6NoUserFound
                elif code.status_code == 400:
                    raise InvalidPlatform
                elif code.status_code == 429:
                    raise TooManyRequests
            data = generic.json()
            ranked_data = data['stats']['queue']['ranked']
            seasonal_data = seasonal.json()
            current_season = next(iter(seasonal_data['seasons']))
            current_seasonal_data = seasonal_data['seasons'][current_season]['regions']['ncsa'][0]
            data_dict = {'username': data['username'], 'platform': data['platform'],
                         'level': data['progression']['level'], 'rank_text': current_seasonal_data['rank_text'],
                         'rank_image': current_seasonal_data['rank_image'], 'mmr': current_seasonal_data['mmr'],
                         'deaths': ranked_data['deaths'], 'kills': ranked_data['kills'],
                         'games_played': ranked_data['games_played'], 'kd': ranked_data['kd'],
                         'wins': ranked_data['wins'], 'losses': ranked_data['losses'], 'wl':  ranked_data['wl']}
            current_unix_time = int(time())
            data_dict['last_update'] = current_unix_time
            self.stats_db_handler.add_ranked_stats(data_dict)
            logger.debug("Scraped stats for user {} on the platform {}".format(username, platform))
        else:
            return

    def get_ranked_stats(self, username, platform):
        try:
            data = self.stats_db_handler.get_ranked_stats(username, platform)
        except DataNotFound:
            raise R6NoUserFound
        return data
