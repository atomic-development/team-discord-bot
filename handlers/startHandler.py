from os import path
from os import mkdir
from handlers import logger


class StartHandler:  # A class containing all the checks to ensure the bot starts successfully

    @staticmethod
    def start():  # calls all the functions within the class
        StartHandler.check_data_directory()
        StartHandler.check_token_directory()
        StartHandler.check_config_json()

    @staticmethod
    def check_data_directory():  # checks the data directory exists
        if not path.exists("./Data/"):
            mkdir("./Data/")
            logger.warning("Data directory could not be found, making one for you!")

    @staticmethod
    def check_token_directory():  # Checks the token directory exists
        if not path.exists("./Tokens"):
            mkdir("./Tokens")
            logger.warning("Tokens directory could not be found, making one for you!")
    
    @staticmethod
    def check_config_json():  # Checks if the config json exists
        if not path.exists("./Data/config.json"):
            config = open("./Data/config.json", "w")
            config.write("{}")
            config.close()
            logger.warning("Could not find config,json, creating one for you!")
