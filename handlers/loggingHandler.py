import logging
from os import path
from os import mkdir
from os import remove

# checks to see if the logs directory exists and removes old logs
if not path.exists("./logs"):
    mkdir("./logs")
if path.exists("./logs/root.log"):
    remove("./logs/root.log")

# create handlers
console_handler = logging.StreamHandler()
file_handler = logging.FileHandler("./logs/root.log")

# set the levels of the handlers
file_handler.setLevel(logging.DEBUG)
console_handler.setLevel(logging.INFO)

# Defining and setting formatter
formatter = logging.Formatter("[%(asctime)s][%(levelname)s]: %(message)s")
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

logger = logging.getLogger(__name__)

# Adding handlers to logger
logger.addHandler(console_handler)
logger.addHandler(file_handler)
# sets logger level to INFO
logger.setLevel(logging.INFO)
