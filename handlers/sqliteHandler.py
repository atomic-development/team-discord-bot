import sqlite3
from time import time

from handlers import logger
from exceptions import DataNotFound


class GuildDataHandler:

    def __init__(self):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # creates guilds table if it does not exist
        cursor.execute("""CREATE TABLE IF NOT EXISTS guilds(
            GuildID text,
            Prefix text)""")
        connection.commit()  # commits the changes to the sqlite database if guilds table is made
        cursor.close()
        connection.close()

    @staticmethod
    def add_guild_id(guild_id):  # Returns True if the GuildID is successfully inserted, otherwise returns False
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # Selects GuildID from guilds table
        cursor.execute("SELECT * FROM guilds WHERE GuildID='{}'".format(guild_id))
        selection = cursor.fetchall()

        # If the GuildID was not found, it will insert it into the guilds table
        if not selection:
            cursor.execute("INSERT INTO guilds ('GuildID') VALUES ('{}')".format(guild_id))
            connection.commit()
            logger.info("Successfully inserted Guild ID '{}' into guilds table".format(guild_id))
            cursor.close()
            connection.close()
            return True

        # If the GuildID was found in the guilds table, it will return false
        if selection:
            logger.info("Could not insert Guild ID '{}' info guilds table, it already exists.".format(guild_id))
            cursor.close()
            connection.close()
            return False

    @staticmethod
    def change_guild_prefix(guild_id, prefix):  # Returns True if prefix is changed, otherwise returns False
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # Selects GuildID from guilds table
        cursor.execute("SELECT * FROM guilds WHERE GuildID='{}'".format(guild_id))
        selection = cursor.fetchall()

        # If the GuildID is found, update the prefix and returns True
        if selection:
            cursor.execute("UPDATE guilds SET Prefix='{0}' WHERE GuildID='{1}'".format(prefix, guild_id))
            connection.commit()
            logger.info("Successfully updated prefix for the GuildID '{}'".format(guild_id))
            cursor.close()
            connection.close()
            return True

        # If the GuildID is not found, return False
        else:
            logger.info("Failed to insert prefix into guilds table, could not find guild_id in table")
            cursor.close()
            connection.close()
            return False

    def remove_guild_data(self, guild_id):  # Returns True if guild_data is deleted, otherwise returns False
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # Selects GuildID from guilds table
        cursor.execute("SELECT * FROM guilds WHERE GuildID='{}'".format(guild_id))
        selection = cursor.fetchall()

        # If the GuildID is found, delete the data and return True
        if selection:
            cursor.execute("DELETE FROM guilds WHERE GuildID='{}'".format(guild_id))
            connection.commit()
            logger.info("Successfully deleted guild data for the GuildID '{}' in the guilds table".format(guild_id))
            cursor.close()
            connection.close()
            return True
        # If the GuildID is not found, return False
        else:
            logger.info("Failed to delete guild data for the GuildID '{}', guild does not exist".format(guild_id))
            cursor.close()
            connection.close()
            return False

    def get_guild_prefix(self, guild_id):  # Returns custom prefix if guild_id is found, otherwise returns default '-'
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # Selects GuildID from guilds table
        cursor.execute("SELECT * FROM guilds WHERE GuildID='{}'".format(guild_id))
        selection = cursor.fetchall()

        # If the GuildID is found, pull and return the prefix
        if selection:
            cursor.execute("SELECT * FROM guilds WHERE GuildID='{}'".format(guild_id))
            cursor.close()
            connection.close()
            return selection[0][1]
        else:
            logger.info("Could not find the prefix for the guild_id '{}'".format(guild_id))
            self.add_guild_id(guild_id)
            self.change_guild_prefix(guild_id, "-")
            cursor.close()
            connection.close()
            return '-'


class MessageDataHandler:

    def __init__(self):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        # Creates the message table if it does not exist
        cursor.execute("""CREATE TABLE IF NOT EXISTS messages(
            GuildID text,
            UserID text,
            ChannelID text,
            NumOfMsgs integer
        )""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS TotalMessages(
        GuildID text,
        UserID text,
        NumOfMsgs integer,
        LastUpdate integer
        )""")

        connection.commit()
        cursor.close()
        connection.close()

    def add_message(self, guild_id, user_id, channel_id):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        number_of_messages = self.get_num_of_msgs(guild_id, user_id, channel_id)

        if number_of_messages == 0:
            cursor.execute("INSERT INTO messages (GuildID, UserID, ChannelID, NumOfMsgs) VALUES (?, ?, ?, 1)",
                           (guild_id, user_id, channel_id))
            logger.debug(
                "Inserted GuildID '{}', UserID '{}' and ChannelID '{}' into messages table".format(guild_id, user_id,
                                                                                                   channel_id))
            connection.commit()
            cursor.close()
            connection.close()
            return True

        else:
            number_of_messages += 1
            cursor.execute("UPDATE messages SET NumOfMsgs=? WHERE (GuildID=? AND UserID=? AND ChannelID=?)",
                           (number_of_messages, guild_id, user_id, channel_id))
            logger.debug(
                "Updated number of messages where GuildID='{}', UserID='{}' and ChannelID='{}' to {}".format(guild_id,
                                                                                                             user_id,
                                                                                                             channel_id,
                                                                                                             number_of_messages))
            connection.commit()
            cursor.close()
            connection.close()
            return True

    def get_num_of_msgs(self, guild_id, user_id, channel_id):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()
        cursor.execute("SELECT COUNT(*) FROM messages WHERE (GuildID=? AND UserID=? AND ChannelID=?)",
                       (guild_id, user_id, channel_id))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.close()
            connection.close()
            return 0
        else:
            cursor.execute("SELECT NumOfMsgs FROM messages WHERE (GuildID=? AND UserID=? AND ChannelID=?)",
                           (guild_id, user_id, channel_id))
            selection = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return selection

    @staticmethod
    def update_total_messages(guild_id, user_id):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM TotalMessages WHERE GuildID='{}' AND UserID='{}'".format(
            guild_id, user_id
        ))
        exists = cursor.fetchone()[0]
        cursor.execute("SELECT NumOfMsgs FROM Messages WHERE GuildID='{}' AND UserID='{}'".format(
            guild_id, user_id
        ))
        user_messages = cursor.fetchall()
        total_user_messages = 0
        for i in range(len(user_messages)):
            total_user_messages += user_messages[i][0]
        current_unix_time = int(time())
        if exists == 0:
            cursor.execute("""INSERT INTO TotalMessages (GuildID, UserID, NumOfMsgs, LastUpdate)
            VALUES ('{}', '{}', {}, {})""".format(
                guild_id, user_id, total_user_messages, current_unix_time
            ))
            connection.commit()
            cursor.close()
            connection.close()
        else:
            cursor.execute("""UPDATE TotalMessages SET NumOfMsgs={}, LastUpdate={} WHERE GuildID='{}' 
            AND UserID='{}'""".format(
                total_user_messages, current_unix_time, guild_id, user_id
            ))
            connection.commit()
            cursor.close()
            connection.close()

    @staticmethod
    def get_total_messages(guild_id, user_id):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM TotalMessages WHERE GuildID='{}' AND UserID='{}'".format(
            guild_id, user_id
        ))
        exists = cursor.fetchone()[0]
        if exists == 0:
            raise DataNotFound
        else:
            cursor.execute("SELECT NumOfMsgs FROM TotalMessages WHERE GuildID='{}' AND UserID='{}'".format(
                guild_id, user_id
            ))
            num_of_msgs = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return num_of_msgs

    @staticmethod
    def get_last_message_update(guild_id, user_id):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM TotalMessages WHERE GuildID='{}' AND UserID='{}'".format(
            guild_id, user_id
        ))
        exists = cursor.fetchone()[0]

        if exists == 0:
            raise DataNotFound
        else:
            cursor.execute("SELECT LastUpdate FROM TotalMessages WHERE GuildID='{}' AND UserID='{}'".format(
                guild_id, user_id
            ))
            last_update = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return last_update


class RainbowStatisticsDatabaseHandler:

    def __init__(self):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        # Creates casual statistic table: (If it does not exist)
        cursor.execute("""CREATE TABLE IF NOT EXISTS RainbowStatsCasual(
        Username text,
        Platform text,
        Level integer,
        Deaths integer,
        Kills integer,
        GamesPlayed integer,
        KD real,
        Wins integer,
        Losses integer,
        WL real,
        LastUpdate integer
        )""")

        # Creates unranked statistics table: (if it does not exist)
        cursor.execute("""CREATE TABLE IF NOT EXISTS RainbowStatsUnranked(
        Username text,
        Platform text,
        Level integer,
        Deaths integer,
        Kills integer,
        GamesPlayed integer,
        KD real,
        Wins integer,
        Losses integer,
        WL real,
        LastUpdate integer
        )""")

        # Creates ranked statistics table: (if it does not exist)
        cursor.execute("""CREATE TABLE IF NOT EXISTS RainbowStatsRanked(
        Username text,
        Platform text,
        Level integer,
        RankText text,
        RankImage text,
        MMR integer,
        Deaths integer,
        Kills integer,
        GamesPlayed integer,
        KD real,
        Wins integer,
        Losses integer,
        WL real,
        LastUpdate integer
        )""")

        connection.commit()
        cursor.close()
        connection.close()

    @staticmethod
    def add_casual_stats(data: dict):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsCasual WHERE (Username=? AND Platform=?)",
                       (data['username'], data['platform']))
        exists = cursor.fetchone()[0]
        if exists == 0:
            with open("./sql_scripts/insert_casual_stats.sql", "r") as f:
                query = f.read()
                cursor.execute(query, (data['username'], data['platform'], data['level'], data['deaths'],
                                       data['kills'], data['games_played'], data['kd'], data['wins'], data['losses'],
                                       data['wl'], data['last_update']))
            logger.debug("Added scraped data to RainbowStatsCasual table")
            connection.commit()
            cursor.close()
            connection.close()
            return
        else:
            with open("./sql_scripts/update_casual_stats.sql", "r") as f:
                query = f.read()
                cursor.execute(query, (data['deaths'], data['kills'], data['level'], data['games_played'], data['kd'],
                                       data['wins'], data['losses'], data['wl'], data['last_update'], data['username'],
                                       data['platform']))
                connection.commit()
                logger.debug("Updated scraped data to RainbowStatsCasual table")
                cursor.close()
                connection.close()
                return

    @staticmethod
    def get_last_casual_scrape(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsCasual WHERE (Username=? AND Platform=?)",
                       (username, platform))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.close()
            connection.close()
            return None
        else:
            cursor.execute("SELECT LastUpdate FROM RainbowStatsCasual WHERE (Username=? AND Platform=?)",
                           (username, platform))
            selection = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return selection

    @staticmethod
    def get_casual_stats(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsCasual WHERE (Username=? AND Platform=?)",
                       (username, platform))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.close()
            connection.close()
            return None
        else:
            cursor.execute("SELECT * FROM RainbowStatsCasual WHERE (Username=? AND Platform=?)",
                           (username, platform))
            stats = cursor.fetchone()
            cursor.close()
            connection.close()
            return stats

    @staticmethod
    def add_unranked_stats(data: dict):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsUnranked WHERE (Username=? AND Platform=?)",
                       (data['username'], data['platform']))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.execute("""INSERT INTO RainbowStatsUnranked (Username, Platform, Level, Deaths, Kills, GamesPlayed,
            KD, Wins, Losses, WL, LastUpdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                           (data['username'], data['platform'], data['level'], data['deaths'], data['kills'],
                            data['games_played'], data['kd'], data['wins'], data['losses'], data['wl'],
                            data['last_update']))
            connection.commit()
            logger.debug("Added scraped data to RainbowStatsUnranked table")
            cursor.close()
            connection.close()
            return
        else:
            cursor.execute("""UPDATE RainbowStatsUnranked SET Deaths=?, Kills=?, Level=?, GamesPlayed=?,
            KD=?, Wins=?, Losses=?, WL=?, LastUpdate=? WHERE Username=? AND Platform=?""",
                           (data['deaths'], data['kills'], data['level'], data['games_played'],
                            data['kd'], data['wins'], data['losses'], data['wl'], data['last_update'],
                            data['username'], data['platform']))
            connection.commit()
            logger.debug("Updated scraped data in RainbowStatsUnranked table")
            cursor.close()
            connection.close()
            return

    @staticmethod
    def get_last_unranked_scrape(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("""SELECT COUNT(*) FROM RainbowStatsUnranked WHERE (Username=? AND Platform=?)""",
                       (username, platform))
        exists = cursor.fetchone()[0]

        if exists == 0:
            cursor.close()
            connection.close()
            raise DataNotFound
        else:
            cursor.execute("""SELECT LastUpdate FROM RainbowStatsUnranked WHERE (Username=? AND Platform=?)""",
                           (username, platform))
            selection = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return selection

    @staticmethod
    def get_unranked_stats(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("""SELECT COUNT(*) FROM RainbowStatsUnranked WHERE (Username=? AND Platform=?)""",
                       (username, platform))
        exists = cursor.fetchone()[0]

        if exists == 0:
            cursor.close()
            connection.close()
            raise DataNotFound
        else:
            cursor.execute("SELECT * FROM RainbowStatsUnranked WHERE (Username=? AND Platform=?)",
                           (username, platform))
            stats = cursor.fetchone()
            cursor.close()
            connection.close()
            return stats

    @staticmethod
    def add_ranked_stats(data: dict):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsRanked WHERE (Username=? AND Platform=?)",
                       (data['username'], data['platform']))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.execute("""INSERT INTO RainbowStatsRanked (Username, Platform, Level, RankText, RankImage, MMR,
            Deaths, Kills, GamesPlayed, KD, Wins, Losses, WL, LastUpdate) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
            ?, ?, ?)""", (data['username'], data['platform'], data['level'], data['rank_text'], data['rank_image'],
                          data['mmr'], data['deaths'], data['kills'], data['games_played'], data['kd'], data['wins'],
                          data['losses'], data['wl'], data['last_update']))
            connection.commit()
            logger.debug("Inserted scraped stats into ranked table")
            cursor.close()
            connection.close()
        else:
            cursor.execute("""UPDATE RainbowStatsRanked SET Level={}, RankText='{}', RankImage='{}', MMR={},
            Deaths={}, Kills={}, GamesPlayed={}, KD={}, Wins={}, Losses={}, WL={}, LastUpdate={} WHERE 
            Username='{}' AND Platform='{}'""".format(data['level'], data['rank_text'], data['rank_image'],
                                                     data['mmr'], data['deaths'], data['kills'], data['games_played'],
                                                     data['kd'], data['wins'], data['losses'], data['wl'],
                                                     data['last_update'], data['username'], data['platform']))
            connection.commit()
            logger.debug("Updated scraped stats in ranked table")
            cursor.close()
            connection.close()

    @staticmethod
    def get_last_ranked_scrape(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsRanked WHERE (Username=? AND Platform=?)",
                       (username, platform))
        exists = cursor.fetchone()[0]
        if exists == 0:
            cursor.close()
            connection.close()
            raise DataNotFound
        else:
            cursor.execute("SELECT LastUpdate FROM RainbowStatsRanked WHERE (Username=? AND Platform=?)",
                           (username, platform))
            selection = cursor.fetchone()[0]
            cursor.close()
            connection.close()
            return selection

    @staticmethod
    def get_ranked_stats(username, platform):
        connection = sqlite3.connect("./Data/data.db")
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM RainbowStatsRanked WHERE (Username=? AND Platform=?)",
                       (username, platform))
        exists = cursor.fetchone()[0]

        if exists == 0:
            cursor.close()
            connection.close()
            raise DataNotFound
        else:
            cursor.execute("SELECT * FROM RainbowStatsRanked WHERE (Username=? AND Platform=?)",
                           (username, platform))
            stats = cursor.fetchone()
            cursor.close()
            connection.close()
            return stats


guild_data = GuildDataHandler()
