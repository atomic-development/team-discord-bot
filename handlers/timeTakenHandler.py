from datetime import datetime


class TimeTakenHandler:  # Handler for the time taken in the footers of embeds

    def __init__(self):  # Defines the instance variables, start_time and end_time
        self.start_time = None
        self.end_time = None

    def start(self):  # Gets the current time at the time of which the method is called
        self.start_time = datetime.now()

    def end(self):  # calculates the execution time, returns in ms
        self.end_time = datetime.now()
        time_difference = self.end_time - self.start_time
        execution_time = round(time_difference.total_seconds() * 1000)
        return execution_time
