import discord
from discord.ext import commands
from handlers import TimeTakenHandler
from handlers import logger
import json


class InviteCommand(commands.Cog):

    def __init__(self, client):
        self.client = client
        logger.info("Invite command loaded successfully")
    
    @commands.command()
    async def invite(self, ctx):
        time = TimeTakenHandler()
        time.start()
        with open("./Data/config.json") as f:
            config = json.load(f)
        try:
            discord_invite = config['bot_invite']
            embed = discord.Embed(title="Bot Invite Link:", description=discord_invite, colour=discord.Colour.green())
            time_taken = time.end()
            embed.set_footer(text="Time taken: {}ms".format(str(time_taken)))
            await ctx.send(embed=embed)
        except KeyError:
            embed = discord.Embed(title="Command not setup:",
                                  description="This command has not been setup in the config, please check the"
                                              "bot wiki for more information",
                                  colour=discord.Colour.red())
            time_taken = time.end()
            embed.set_footer(text="Time taken: {}ms".format(str(time_taken)))
            await ctx.send(embed=embed)


def setup(client):
    client.add_cog(InviteCommand(client))
