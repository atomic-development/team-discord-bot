import discord
from discord.ext import commands
from handlers import logger
from handlers import TimeTakenHandler


class PingCommand(commands.Cog):  # A command used to find the bot latency

    def __init__(self, client):
        self.client = client
        logger.info("Ping command loaded successfully")
    
    @commands.command()
    async def ping(self, ctx):
        time = TimeTakenHandler()  # Instance of time taken handler
        time.start()
        async with ctx.channel.typing():
            embed = discord.Embed(title="Pong:", description=f"Latency: {round(self.client.latency * 1000)}ms", colour=discord.Colour.green())  # Adds the latency to an embed
            time_taken = time.end()  # gets time execution time
            total_time_taken = round(time_taken - self.client.latency * 1000)  # removes the bot latency from the execution time
            embed.set_footer(text="Time taken: {}ms".format(str(total_time_taken)))  # adds the total execution time to the footer
            await ctx.send(embed=embed)


def setup(client):
    client.add_cog(PingCommand(client))
