import discord
from discord.ext import commands

from bot import version
from handlers import MessageDataHandler
from handlers import TimeTakenHandler
from handlers import guild_data
from handlers import logger


class Events(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.message_data = MessageDataHandler()
        logger.info("Events loaded successfully")

    @commands.Cog.listener()
    async def on_ready(self):  # Listener triggered when the bot is ready to handle commands
        logger.info("Signed in as {}!".format(self.client.user))
        await self.client.change_presence(status=discord.Status.idle,
                                          activity=discord.Game("Team bot | Version {}".format(version)))

    @commands.Cog.listener()
    async def on_guild_join(self, guild):  # Listener triggered when the bot joins a discord server
        guild_data.add_guild_id(guild.id)
        guild_data.change_guild_prefix(guild.id, prefix="-")
        logger.info("Bot joined discord: {} ID: {}".format(guild.name, guild.id))

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):  # Listener triggered when bot leaves a discord server
        guild_data.remove_guild_data(guild.id)
        logger.info("Bot left discord: {} ID: {}".format(guild.name, guild.id))

    # @commands.Cog.listener()
    # async def on_command_error(self, ctx, error):  # Listener trigger when a command fails
    #
    #     if hasattr(ctx.command, 'on_error'):  # returns if the error has been handled locally on the command
    #         return
    #
    #     if isinstance(error, commands.CommandNotFound):  # Checks to see if the error is a CommandNotFound error
    #
    #         # Make embed
    #         embed = discord.Embed(title="An error has occurred:",
    #                               description="That command does not exist!",
    #                               colour=discord.Colour.red())
    #         await ctx.send(embed=embed)  # sends the embed in discord
    #
    #     else:
    #         logger.warning(error)

    @commands.Cog.listener()
    async def on_message(self, message):

        if message.author.bot:
            return

        else:
            time = TimeTakenHandler()
            time.start()
            is_successful = self.message_data.add_message(message.guild.id, message.author.id, message.channel.id)
            if is_successful:
                time_taken = time.end()
                logger.debug("Successfully added message to messages table, took {}ms".format(time_taken))
                return
            else:
                time_taken = time.end()
                logger.warning("Failed to add message to the messages table, took {}ms".format(time_taken))


def setup(client):
    client.add_cog(Events(client))
