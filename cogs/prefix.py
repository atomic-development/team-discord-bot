import discord
from discord.ext import commands
from handlers import guild_data
from handlers import logger


class PrefixCommand(commands.Cog):

    def __init__(self, client):
        self.client = client
        logger.info("Prefix command loaded successfully")

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def prefix(self, ctx, prefix: str):
        guild_data.change_guild_prefix(ctx.guild.id, prefix)
        embed = discord.Embed(title="Prefix Changed Successfully:",
                              description="The prefix has been changed to **{}**".format(prefix),
                              colour=discord.Colour.green())
        embed.set_footer(text="Prefix changed by {}".format(ctx.author))
        await ctx.send(embed=embed)

    @prefix.error
    async def command_error_prefix(self, ctx, error):
        embed = discord.Embed()
        if isinstance(error, commands.MissingPermissions):
            embed.title = "Invalid Permissions:"
            embed.description = "You do not have permission to use this command, command requires administrator!"
            embed.colour = discord.Colour.red()
            embed.set_footer(text="{} attempted to use the prefix command".format(ctx.author))
            await ctx.send(embed=embed)
            return
        else:
            pass


def setup(client):
    client.add_cog(PrefixCommand(client))
