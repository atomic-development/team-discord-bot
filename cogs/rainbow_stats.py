import discord
from discord.ext import commands

from exceptions import InvalidPlatform
from exceptions import NoR6Token
from exceptions import R6NoUserFound
from exceptions import TooManyRequests
from handlers import R6StatsHandler
from handlers import logger


class RainbowStatsCommand(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.r6_stats_handler = R6StatsHandler()
        logger.info("Rainbow Six Siege Stats loaded successfully")

    # Raises R6NoUserFound and NoR6Token
    @commands.command(aliases=["r6"])
    async def r6stats(self, ctx, username, platform, stats_type: str = "all", *args: list):
        async with ctx.channel.typing():

            if stats_type.lower() == "casual":
                embed = discord.Embed(title="Casual Statistics for {}:".format(username),
                                      colour=discord.Colour.blue())
                try:
                    self.r6_stats_handler.scrape_casual_stats(username, platform)

                except Exception as error:
                    embed.title = "An error has occurred:"
                    embed.colour = discord.Colour.red()

                    if isinstance(error, NoR6Token):
                        embed.description = "This command is not currently setup as there is no R6Token!"
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, R6NoUserFound):
                        embed.title = "No User Found:"
                        embed.description = "The user {} could not be found on the platform {}".format(
                            username, platform
                        )
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, InvalidPlatform):
                        embed.title = "Invalid Platform:"
                        embed.description = "There is no platform known as {}, valid platforms include: pc, psn, xbox".format(
                            platform
                        )
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, TooManyRequests):
                        embed.title = "Too Many Requests:"
                        embed.description = "There are currently too many requests, please get in contact with us!"
                        await ctx.send(embed=embed)
                        return

                    else:
                        logger.error(error, exc_info=True)
                        return

                try:
                    stats = self.r6_stats_handler.get_casual_stats(username, platform)
                except R6NoUserFound:
                    embed.title = "No User Found:"
                    embed.description = "The user {} could not be found on the platform {}".format(
                        username, platform
                    )
                    embed.colour = discord.Colour.red()
                    await ctx.send(embed=embed)
                    return
                embed.add_field(name="**User:**",
                                value="Username: **{}**\nPlatform: **{}**\nLevel: **{}**".format(
                                    stats[0], stats[1], stats[2]))
                embed.add_field(name="**Casual:**",
                                value="Number of Matches: **{}**\nKills: **{}**\nDeaths: **{}**\nKill/Death: **{}**\nWins: **{}**\nLosses: **{}**\nWin/Loss: **{}**".format(
                                    str(stats[5]), str(stats[4]), str(stats[3]), str(stats[6]), str(stats[7]),
                                    str(stats[8]), str(stats[9])))
                next_scrape = self.r6_stats_handler.remaining_time(username=username, platform=platform,
                                                                   type=stats_type.lower())
                embed.set_footer(text="Next scrape in {} (Hours/Minutes/Seconds)".format(str(next_scrape)))
                await ctx.send(embed=embed)

            elif stats_type.lower() == "unranked":
                embed = discord.Embed(title="Unranked Statistics for {}:".format(username),
                                      colour=discord.Colour.dark_blue())
                try:
                    self.r6_stats_handler.scrape_unranked_stats(username, platform)

                except Exception as error:
                    embed.title = "An error has occurred:"
                    embed.colour = discord.Colour.red()

                    if isinstance(error, NoR6Token):
                        embed.description = "This command is not currently setup as there is no R6Token!"
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, R6NoUserFound):
                        embed.title = "No User Found:"
                        embed.description = "The user {} could not be found on the platform {}".format(
                            username, platform
                        )
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, InvalidPlatform):
                        embed.title = "Invalid Platform:"
                        embed.description = "There is no platform known as {}, valid platforms include: pc, psn, xbox".format(
                            platform
                        )

                    elif isinstance(error, TooManyRequests):
                        embed.title = "Too Many Requests:"
                        embed.description = "There are currently too many requests, please get in contact with us!"
                        return

                    else:
                        logger.error(error, exc_info=True)
                        return
                    await ctx.send(embed=embed)
                    return

                stats = self.r6_stats_handler.get_unranked_stats(username, platform)
                embed.add_field(name="**User:**",
                                value="Username: **{}**\nPlatform: **{}**\nLevel: **{}**".format(
                                    stats[0], stats[1], stats[2]))
                embed.add_field(name="**Casual:**",
                                value="Number of Matches: **{}**\nKills: **{}**\nDeaths: **{}**\nKill/Death: **{}**\nWins: **{}**\nLosses: **{}**\nWin/Loss: **{}**".format(
                                    str(stats[5]), str(stats[4]), str(stats[3]), str(stats[6]), str(stats[7]),
                                    str(stats[8]), str(stats[9])))
                next_scrape = self.r6_stats_handler.remaining_time(username=username, platform=platform,
                                                                   type=stats_type.lower())
                embed.set_footer(text="Next scrape in {} (Hours/Minutes/Seconds)".format(str(next_scrape)))
                await ctx.send(embed=embed)

            elif stats_type.lower() == "ranked":
                embed = discord.Embed(title="Ranked Statistics for {}".format(username),
                                      colour=discord.Colour.purple())

                try:
                    self.r6_stats_handler.scrape_ranked_stats(username, platform)

                except Exception as error:
                    embed.title = "An error has occurred:"
                    embed.colour = discord.Colour.red()

                    if isinstance(error, NoR6Token):
                        embed.description = "This command is not currently setup as there is no R6Token!"
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, R6NoUserFound):
                        embed.title = "No User Found:"
                        embed.description = "The user {} could not be found on the platform {}".format(
                            username, platform
                        )
                        await ctx.send(embed=embed)
                        return

                    elif isinstance(error, InvalidPlatform):
                        embed.title = "Invalid Platform:"
                        embed.description = "There is no platform known as {}, valid platforms include: pc, ps4, xbox".format(
                            platform
                        )

                    elif isinstance(error, TooManyRequests):
                        embed.title = "Too Many Requests:"
                        embed.description = "There are currently too many requests, please get in contact with us!"
                        return

                    else:
                        logger.error(error, exc_info=True)
                        return
                    await ctx.send(embed=embed)
                    return
                stats = self.r6_stats_handler.get_ranked_stats(username, platform)
                embed.add_field(name="**User:**",
                                value="""Username: **{}**\nPlatform: **{}**\nLevel: **{}**\nRank: **{}**, 
MMR: **{}**""".format(stats[0], stats[1], stats[2], stats[3], stats[5]), inline=False)
                embed.add_field(name="**Ranked:**",
                                value="""Number of Matches: **{}**\nKills: **{}**\nDeaths: **{}**\nKill/Death: **{}**, 
Wins: **{}**\nLosses: **{}**\nWin/Loss: **{}**""".format(stats[8], stats[7],
                                                                                         stats[6], stats[9],
                                                                                         stats[10], stats[11],
                                                                                         stats[12]),
                                inline=False)
                rank_url = stats[4]
                rank_png_url = rank_url.replace("ranks", "rank-imgs").replace(".svg", ".png")
                embed.set_thumbnail(url=rank_png_url)
                next_scrape = self.r6_stats_handler.remaining_time(username, platform, type=stats_type.lower())
                embed.set_footer(text="Next scrape in {} (Hours/Minutes/Seconds)".format(str(next_scrape)))
                await ctx.send(embed=embed)
            elif stats_type.lower() == "all":
                pass
            else:
                embed = discord.Embed(title="An error has occurred:",
                                      description="There is no statistic type known as '{}'".format(stats_type),
                                      colour=discord.Colour.red())
                await ctx.send(embed=embed)


def setup(client):
    client.add_cog(RainbowStatsCommand(client))
