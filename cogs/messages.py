import discord
from discord.ext import commands

from time import time as current_time
from handlers import MessageDataHandler
from handlers import TimeTakenHandler
from handlers import logger
from exceptions import DataNotFound
from datetime import timedelta


class MessageHandler(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.message_data = MessageDataHandler()
        self.UPDATE_INTERVAL = 120
        logger.info("Messages stats loaded successfully")

    @commands.command(name="messages", aliases=['msgs'])
    async def messages(self, ctx, command_type="channel", username: discord.User = None, channel: discord.TextChannel = None):
        time = TimeTakenHandler()
        time.start()
        embed = discord.Embed(colour=discord.Colour.green())

        if command_type.lower() == "all":  # total number of messages in the discord server
            user_id = ""
            try:
                if username is None:
                    username = ctx.author
                    user_id = ctx.author.id
                    last_update = self.message_data.get_last_message_update(ctx.guild.id, ctx.author.id)

                else:
                    user_id = username.id
                    last_update = self.message_data.get_last_message_update(ctx.guild.id, username.id)
            except DataNotFound:
                last_update = 0

            current_unix_time = int(current_time())
            if current_unix_time >= last_update + self.UPDATE_INTERVAL:
                self.message_data.update_total_messages(ctx.guild.id, user_id)
            total_messages = self.message_data.get_total_messages(ctx.guild.id, user_id)
            embed.title = "Total messages sent by {}:".format(username)
            embed.description = "**{}** has sent a total of **{}** messages in this discord server!".format(
                username, total_messages
            )
            thumbnail = username.avatar_url
            embed.set_thumbnail(url=thumbnail)
            next_update = self.calculate_update_time(ctx.guild.id, user_id)
            time_taken = time.end()
            embed.set_footer(text="Time taken: {}ms | Next update: {} (Messages/Seconds)".format(
                time_taken, next_update
            ))
            await ctx.send(embed=embed)

        elif command_type.lower() == "channel":  # number of messages in specific channel

            if username is None:
                username = ctx.author
                channel = ctx.channel
            elif channel is None:
                channel = ctx.channel

            messages = self.message_data.get_num_of_msgs(ctx.guild.id, username.id, channel.id)
            embed.title = "Messages sent by {} in the channel #{}".format(
                username, channel
            )
            embed.description = "**{}** has sent **{}** messages in the channel **{}**!".format(
                username, messages, channel
            )
            thumbnail = username.avatar_url
            embed.set_thumbnail(url=thumbnail)
            time_taken = time.end()
            embed.set_footer(text="Time taken: {}ms".format(time_taken))
            await ctx.send(embed=embed)

        elif command_type.lower() == "delete" or command_type.lower() == "reset":  # Reset messages -> admin command
            pass

    @messages.error
    async def command_error_messages(self, ctx, error):

        if isinstance(error, commands.UserNotFound):
            embed = discord.Embed(title="An error has occurred:",
                                  description="The user you have requested does not exist, please ensure you @ them",
                                  colour=discord.Colour.red())
            embed.set_footer(text="Message statistics requested by {} and failed!".format(ctx.author))
            await ctx.send(embed=embed)

        if isinstance(error, commands.ChannelNotFound):
            embed = discord.Embed(title="An error has occurred:",
                                  description="The channel you have requested could not be found",
                                  colour=discord.Colour.red())
            embed.set_footer(text="Message statistics requested by {} and failed!".format(ctx.author))
            await ctx.send(embed=embed)
        else:
            logger.error(error, exc_info=True)

    def calculate_update_time(self, guild_id, user_id):
        last_update = self.message_data.get_last_message_update(guild_id, user_id)
        current_unix_time = int(current_time())
        remaining_time = (last_update + self.UPDATE_INTERVAL) - current_unix_time
        datetime = timedelta(seconds=remaining_time)
        return datetime


def setup(client):
    client.add_cog(MessageHandler(client))
