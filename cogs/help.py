import discord
from discord.ext import commands
from handlers import logger


class HelpCommand(commands.Cog):

    def __init__(self, client):
        self.client = client
        logger.info("Help command loaded successfully")

    @commands.command()
    async def help(self, ctx):
        embed = discord.Embed(title="Commands:",
                              description="Currently the help command has not been fully finished, please use the wiki"
                                          "here: https://gitlab.com/AtomicProgramming/team-discord-bot/-/wikis/home",
                              colour=discord.Colour.green())
        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(HelpCommand(client))
