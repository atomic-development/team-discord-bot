import discord
from discord.ext import commands
from os import stat
from handlers import TimeTakenHandler
from handlers import logger


class InfoCommand(commands.Cog):  # A command used to find out information about the discord bot

    def __init__(self, client):
        self.client = client
        logger.info("Info command loaded successfully")

    @commands.command()
    async def info(self, ctx):
        time = TimeTakenHandler()  # Instance of the time taken handler
        time.start()
        embed = discord.Embed(title="Team Bot:", colour=discord.Colour.green())  # creates embed instance
        embed.add_field(name="Owner:", value="Team bot was developed by **Destroyer#4570** (Discord) or"
                                             "**AtomicProgramming** (Gitlab)")  # Adds owner field
        embed.add_field(name="Git Repository:",
                        value="https://gitlab.com/AtomicProgramming/team-discord-bot")  # Adds git repository field
        database_size = stat("./Data/data.db").st_size / 1024
        embed.add_field(name="Bot stats:", value="Database size: **{}KB**".format(database_size))
        time_taken = time.end()  # gets time time taken
        embed.set_footer(text="Time taken: {}ms".format(str(time_taken)))  # adds the time taken as a footer
        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(InfoCommand(client))
