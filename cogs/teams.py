from discord.ext import commands
from handlers import Teams
import discord
from handlers import TimeTakenHandler
from handlers import logger


class TeamsCommand(commands.Cog):

    def __init__(self, client):
        self.client = client
        logger.info("Team command loaded successfully")

    @commands.command(aliases=["t", "team"])
    async def teams(self, ctx, num_of_teams, *args):
        time = TimeTakenHandler()
        time.start()
        args = list(args)
        async with ctx.channel.typing():

            try:
                num_of_teams = int(num_of_teams)
            except ValueError:
                embed = discord.Embed(title="An error has occurred:",
                                      description="Please ensure the number of teams is passed as an integer!",
                                      colour=discord.Colour.red())
                embed.set_footer(text="Teams requested by {} and failed!".format(ctx.message.author))
                await ctx.send(embed=embed)
                return

            if 1 < num_of_teams <= 20:
                pass
            else:
                embed = discord.Embed(title="An error has occurred:",
                                      description="You may only have 2 - 20 teams, not {}".format(str(num_of_teams)))
                embed.set_footer(text="Teams requested by {} and failed!".format(ctx.message.author))
                await ctx.send(embed=embed)
                return

            if len(args) == 0:

                try:
                    voice_channel = ctx.message.author.voice.channel
                except AttributeError:
                    embed = discord.Embed(title="An error has occurred:",
                                          description="You must be in a voice chat in order to use this command without mentioning users!",
                                          colour=discord.Colour.red())
                    embed.set_footer(text="Teams requested by {} and failed!".format(ctx.message.author))
                    await ctx.send(embed=embed)
                members = voice_channel.members
                member_list = []
                for member in members:
                    member_list.append(member.mention)
                sorted_teams = Teams(num_of_teams, member_list).sort_teams()
                embed = discord.Embed(title="Teams:",
                                      colour=discord.Colour.green())
                time_taken = time.end()
                embed.set_footer(text="Time taken: " + str(time_taken) + "ms")
                embed = self.list_to_embed(sorted_teams, embed, num_of_teams)
                await ctx.send(embed=embed)

            else:
                sorted_teams = Teams(num_of_teams, args).sort_teams()
                embed = discord.Embed(title="Teams:",
                                      colour=discord.Colour.green())
                time_taken = time.end()
                embed.set_footer(text="Time taken: " + str(time_taken) + "ms")

                embed = self.list_to_embed(sorted_teams, embed, num_of_teams)
                await ctx.send(embed=embed)

    @teams.error
    async def command_error_teams(self, ctx, error):

        if isinstance(error, commands.MissingRequiredArgument):
            embed = self.error_embed("Please ensure you add the number of teams you desire (between 2 -> 20)")
            await ctx.send(embed=embed)

    @staticmethod
    def error_embed(error_message):
        embed = discord.Embed(title="An error has occurred:",
                              description=error_message,
                              colour=discord.Colour.red())
        return embed

    @staticmethod
    def list_to_embed(users_list, embed, num_of_teams):
        for i in range(1, num_of_teams + 1):
            try:
                team_string = "- " + users_list[i - 1][0]
            except IndexError:
                team_string = None
            for j in users_list[i - 1][1:]:
                team_string += "\n- " + j
            embed.add_field(name="Team " + str(i) + ":", value=team_string)
        return embed


def setup(client):
    client.add_cog(TeamsCommand(client))
